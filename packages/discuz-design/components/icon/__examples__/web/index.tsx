import { Icon } from '@discuzq/design';
import React from 'react';

export default function ImgSrc() {
  return (
    <>
      <Icon name="PersonalOutlined" size={30} />
    </>
  );
}
