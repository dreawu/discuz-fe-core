import React from 'react';
import { Radio } from '@discuzq/design';

export default function CheckboxExample() {
  return (
    <div className="page">
      <div>
        <Radio defaultChecked>Radio</Radio>
      </div>
    </div>
  );
}

