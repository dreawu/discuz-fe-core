import React from 'react';
import { Alert } from '@discuzq/design';

export default function AlertExample() {
  return (
    <div className="page">
      <Alert type="success">success 成功提示的文案</Alert>
      <br />
    </div>
  );
}
